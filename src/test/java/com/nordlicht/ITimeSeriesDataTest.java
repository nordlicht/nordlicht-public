package com.nordlicht;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * @author Tomas Fecko
 */
public class ITimeSeriesDataTest {

    @Test
    public void getTimestamp() {
        assertTrue(new ITimeSeriesData() {
            @Override
            public long getTimestamp() {
                return 1;
            }

            @Override
            public int getByteableClassId() {
                return 0;
            }
        }.compareTo(new ITimeSeriesData(){
            @Override
            public long getTimestamp() {
                return 0;
            }

            @Override
            public int getByteableClassId() {
                return 0;
            }
        }) > 0);
    }
}