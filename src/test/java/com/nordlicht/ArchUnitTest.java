package com.nordlicht;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.CompositeArchRule;
import com.tngtech.archunit.library.GeneralCodingRules;
import org.slf4j.Logger;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.fields;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.methods;

/**
 *
 */
@AnalyzeClasses(
        packages = {
                "com.nordlicht"
        },
        importOptions = {
                ImportOption.DoNotIncludeTests.class,
                ImportOption.DoNotIncludeJars.class
        }
)
public class ArchUnitTest {

    @ArchTest
    static final ArchRule loggersPrivateStaticFinal =
            fields().that().haveRawType(Logger.class)
                    .should().beStatic()
                    .andShould().bePrivate().
                    andShould().beFinal().
                    because("It's our team's agreed convention");
    @ArchTest
    static final ArchRule utilityMethodsPublicStatic =
            methods().that().areDeclaredInClassesThat().
                    resideInAnyPackage("..util..", "..utils..").
                    should().bePublic().
                    andShould().beStatic();
    @ArchTest
    static final ArchRule generalCodingPractices = CompositeArchRule
            .of(GeneralCodingRules.NO_CLASSES_SHOULD_ACCESS_STANDARD_STREAMS)
            .and(GeneralCodingRules.NO_CLASSES_SHOULD_THROW_GENERIC_EXCEPTIONS)
            .and(GeneralCodingRules.NO_CLASSES_SHOULD_USE_JAVA_UTIL_LOGGING)
            .and(GeneralCodingRules.NO_CLASSES_SHOULD_USE_JODATIME)
            .and(GeneralCodingRules.NO_CLASSES_SHOULD_USE_FIELD_INJECTION)
            .because("These are Voilation of general coding rules");


}
