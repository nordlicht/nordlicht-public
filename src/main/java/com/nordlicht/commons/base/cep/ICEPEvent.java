package com.nordlicht.commons.base.cep;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * CEP Event.
 */
public interface ICEPEvent {

    /**
     *
     * @return
     */
    String getQueryId();

    /**
     *
     * @return
     */
    long getCurrentTimestamp();

    /**
     *
     * @return
     */
    long getNextTimestamp();

    /**
     *
     * @return
     */
    List<LinkedHashMap<String, Object>> getInputStreamOfObjectsFromCEP();

    /**
     *
     * @return
     */
    List<LinkedHashMap<String, Object>> getRemoveStreamOfObjectsFromCEP();

    /**
     *
     * @return
     */
    boolean isInsertStreamEmpty();

    /**
     *
     * @return
     */
    boolean isRemoveStreamEmpty();
}






