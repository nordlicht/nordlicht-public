package com.nordlicht.commons.base.cep;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * @author Tomas Fecko
 */
public interface IEventWrapper {


    /**
     *
     * @return current timestamp
     */
    long getCurrentTimestamp();

    /**
     *
     * @return next timestamp
     */
    long getNextTimestamp();

    /**
     *
     * @return input stream
     */
    List<LinkedHashMap<String, Object>> getInputStream();

    /**
     *
     * @return remove stream
     */
    List<LinkedHashMap<String, Object>> getRemoveStream();

    /**
     *
     * @return statement id
     */
    String getStatementId();
}



