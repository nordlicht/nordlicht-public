package com.nordlicht.commons.base.serialization.byteable;

import java.io.Serializable;

/**
 * @deprecated No longer used to mark byteable objects, any object can be byteable as long as it has serializer registered.
 * @author Tomas Fecko
 */
@Deprecated
public interface IByteable extends Serializable {

    /**
     * @deprecated No longer used
     * @return class Id, under which the class is registered in SerializerHandler
     */
    @Deprecated
    int getByteableClassId();
}







