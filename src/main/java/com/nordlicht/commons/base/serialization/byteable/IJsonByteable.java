package com.nordlicht.commons.base.serialization.byteable;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface IJsonByteable extends IByteable {

    int BYTEABLE_ID = 98;

    @JsonIgnore
    @Override
    default int getByteableClassId() {
        // id for JacksonDeserializer
        return BYTEABLE_ID;
    }
}
