package com.nordlicht.commons.base.definitions;

import com.nordlicht.IDefinition;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

/**
 * Definition providers serves as central registry of all registered IDefinition values and it is used to access the IDefinition
 * values by their class and either id or name.
 *
 * @author Tomas Fecko
 */
public interface IDefinitionProvider {

    /**
     *
     * @param pDefinition definition
     */
    <T extends IDefinition> void registerDefinition(Class<T> pDefinitionClass, IDefinition... pDefinition);

    /**
     *
     * @param pType definition
     * @param <T> type of definition
     * @return definitions
     */
    <T extends IDefinition> List<T> getAllDefinitions(Class<T> pType);

    /**
     *
     * @param pType definition
     * @param <T> type of definition
     * @param pDefinitionName definition name
     * @return definition
     */
    @Nullable
    <T extends IDefinition> T getDefinition(Class<T> pType, String pDefinitionName);

    /**
     *
     * @param type type of definition
     * @param definitionName definition name
     * @return optiobnal
     * @param <T> type
     */
    default <T extends IDefinition> Optional<T> getDefinitionOptional(Class<T> type, String definitionName) {
        return Optional.ofNullable(getDefinition(type, definitionName));
    }

    /**
     *
     * @param pType definition
     * @param <T> type of definition
     * @param pDefinitionId definition id
     * @return definition
     */
    @Nullable
    <T extends IDefinition> T getDefinition(Class<T> pType, int pDefinitionId);

    /**
     *
     * @param type type of definition
     * @param definitionId definition id
     * @return optiobnal
     * @param <T> type
     */
     default <T extends IDefinition> Optional<T> getDefinitionOptional(Class<T> type, int definitionId) {
        return Optional.ofNullable(getDefinition(type, definitionId));
    }

    List<Class<?>> getAllRegisteredTypes();
}








