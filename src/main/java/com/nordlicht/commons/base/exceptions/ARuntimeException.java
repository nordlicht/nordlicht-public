package com.nordlicht.commons.base.exceptions;

import java.util.Objects;

import static java.lang.String.valueOf;
import static java.util.UUID.randomUUID;


/**
 * @author Tomas Fecko
 */
public abstract class ARuntimeException extends RuntimeException implements IHasStatusID {

    private final String exceptionUID = valueOf(randomUUID());
    private IExceptionStatus statusID;

    /**
     *
     */
    public ARuntimeException() {
    }

    /**
     * @param pStr message
     * @param pCause cause
     */
    public ARuntimeException(String pStr, Throwable pCause) {
        super(pStr, pCause);
    }

    /**
     * @param pStr message
     */
    public ARuntimeException(String pStr) {
        super(pStr);
    }

    /**
     * @param pCause throwable
     */
    public ARuntimeException(Throwable pCause) {
        super(pCause);
    }

    @Override
    public String getMessage() {
        String message = super.getMessage();
        if (getStatusID() != null) {
            return "[" + getStatusID() + ", " + exceptionUID + "] " + message;
        }
        return "[" + exceptionUID + "] " + message;
    }

    public String getMessageWithoutUuid() {
        return super.getMessage();
    }

    @Override
    public String getUID() {
        return exceptionUID;
    }

    /**
     * @return the statusID
     */
    @Override
    public final IExceptionStatus getStatusID() {
        return statusID;
    }

    /**
     * @param pStatusID status ID enum
     * @return this
     */
    public final ARuntimeException setStatusID(IExceptionStatus pStatusID) {
        if (pStatusID == null) {
            return this;
        }
        statusID = pStatusID;
        return this;
    }

    @Override
    public boolean equals(Object pO) {
        if (this == pO) {
            return true;
        }
        if (pO == null || getClass() != pO.getClass()) {
            return false;
        }
        final ARuntimeException that = (ARuntimeException) pO;
        return statusID == that.statusID
                && Objects.equals(exceptionUID, that.exceptionUID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(exceptionUID, statusID);
    }
}





