package com.nordlicht.commons.base.exceptions;

import java.util.Objects;
import java.util.UUID;


/**
 * @author Tomas Fecko
 */
public abstract class AException extends Exception implements IHasStatusID {

    private final String exceptionUID = UUID.randomUUID().toString();
    private IExceptionStatus statusID;

    /**
     *
     */
    public AException() {
    }

    /**
     * @param pStr message
     * @param pCause cause
     */
    public AException(String pStr, Throwable pCause) {
        super(pStr, pCause);
    }

    /**
     * @param pStr message
     */
    public AException(String pStr) {
        super(pStr);
    }

    /**
     * @param pCause throwable
     */
    public AException(Throwable pCause) {
        super(pCause);
    }

    @Override
    public String getMessage() {
        String message = super.getMessage();
        if (getStatusID() != null) {
            return "[" + getStatusID() + ", " + exceptionUID + "] " + message;
        }
        return "[" + exceptionUID + "] " + message;
    }

    public String getMessageWithoutUuid() {
        return super.getMessage();
    }

    @Override
    public String getUID() {
        return exceptionUID;
    }

    /**
     * @return the statusID
     */
    @Override
    public final IExceptionStatus getStatusID() {
        return statusID;
    }

    /**
     * @param pStatusID status ID enum
     * @return this
     */
    public final AException setStatusID(IExceptionStatus pStatusID) {
        if (pStatusID == null) {
            return this;
        }
        statusID = pStatusID;
        return this;
    }

    @Override
    public boolean equals(Object pO) {
        if (this == pO) {
            return true;
        }
        if (pO == null || getClass() != pO.getClass()) {
            return false;
        }
        final AException that = (AException) pO;
        return statusID == that.statusID
                && Objects.equals(exceptionUID, that.exceptionUID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(exceptionUID, statusID);
    }
}





