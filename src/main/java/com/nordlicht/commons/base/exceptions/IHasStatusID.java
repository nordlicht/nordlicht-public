package com.nordlicht.commons.base.exceptions;

/**
 * Common interface for exceptions having exception status.
 *
 * @author Gabriel Gecy
 */
public interface IHasStatusID {

    /**
     * Getter for exception status. (Still using old name even if type changes
     * for backwards compatibility)
     *
     * @return exception status
     */
    IExceptionStatus getStatusID();

    String getUID();
}
