package com.nordlicht.commons.base.exceptions;

/**
 * Interface represents a way to get a unique status code of exception.
 * @author Tomas Fecko
 */
public interface IExceptionStatus {

    /**
     * Returns exception status ID - unique exception identifier.
     * @return exception status ID
     */
    int getStatusID();

    /**
     * Exception status category, to avoid same id conflicts.
     *
     * @return category
     */
    default String getCategory() {
        return getClass().getSimpleName();
    }

    /**
     * String representation used to display to users.
     *
     * @return string representation
     */
    default String getStringCode() {
        return getCategory() + ":" + getStatusID();
    }
}






