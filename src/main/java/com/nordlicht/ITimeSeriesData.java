package com.nordlicht;

import com.nordlicht.commons.base.serialization.byteable.IByteable;

/**
 * Time series data.
 * @author Tomas Fecko
 */
public interface ITimeSeriesData extends IByteable, Comparable<ITimeSeriesData> {

    /**
     * Method returns timestamp of the this item.
     * @return java millisecond timestamp
     */
    long getTimestamp();

    @Override
    default int compareTo(ITimeSeriesData pTimeSeriesData) {
        return pTimeSeriesData == null ? 1 : Long.compare(getTimestamp(), pTimeSeriesData.getTimestamp());
    }
}






