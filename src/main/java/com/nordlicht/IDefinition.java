package com.nordlicht;

/**
 * Definition SPI. Similar like enums, definitions has unique name and unique int representation. Usually it is
 * extended by more specific definition interface, which could be implemented by multiple classes (usually enums),
 * which values are then registered in IDefinitionProvider as values for the specific interface class. IDefinitionProvider is
 * then used to access the interface values by its type and either id or name. IDefinitionProvider is also used for deserialization
 * of IDefinition values from either byte array using IByteable or from json string using the definition type and id.
 *
 * @param <T> type of definition
 * @author Tomas Fecko
 */
public interface IDefinition<T extends IDefinition> extends ITypeId {

    /**
     * Unspecified definition name.
     */
    String UNSPECIFIED = "UNSPECIFIED";
    String UNRECOGNIZED = "UNRECOGNIZED";
    int UNSPECIFIED_ID = 0;
    int UNRECOGNIZED_ID = 0;

    /**
     *
     * @return true, if value is unspecified
     */
    default boolean isUnspecified() {
        return UNSPECIFIED.equals(this.getName()) || UNRECOGNIZED.equals(this.getName());
    }

    @Override
    boolean equals(Object obj);

    @Override
    int hashCode();
}






