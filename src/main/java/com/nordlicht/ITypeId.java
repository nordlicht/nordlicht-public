package com.nordlicht;

import java.io.Serializable;

/**
 *
 * @author Tomas Fecko
 */
public interface ITypeId extends Serializable {

    /**
     * Unique id.
     * @return name
     */
    int getId();

    /**
     * Unique name.
     * @return name
     */
    String getName();
}






