#!/bin/bash

set -v
################################################################################
#                         Additional script file                               #
#                             for task file                                    #
#                        Created by Marek Urban                                #
#                                                                              #
# This is only partial file of pipeline, for full understanding please look    #
# into specified pipeline.yml file.                                            #
################################################################################
set +v
# set system enviroments
# -e = Exit immediately if a command exits with a non-zero exit status.
# -u = Treat unset variables as an error when substituting.
# -v = Print shell input lines as they are read.
set -e -u

# Uncomment for debug
# set -v -x

# print out Maven version
mvn --version

# Perform mvn clean install for every individual project
# -B = batch mode
# -q = quiet, print out only when error occurs

cd nordlicht-public/
mvn clean install -B -q
mvn dependency:copy-dependencies -B -q

# Wait 3 second to calm down everything...
echo "Waiting to make the job done..."
echo "3 sec"
sleep 1
echo "2 sec"
sleep 1
echo "1 sec"
sleep 1
echo "Done."

# Generate jacoco and surefire reports
echo "Generating jacoco and surefire test reports..."
mvn -DXmx1024m org.jacoco:jacoco-maven-plugin:prepare-agent install -B -q
mvn -DXmx1024m org.jacoco:jacoco-maven-plugin:prepare-agent-integration install -B -q
mvn -DXmx1024m surefire-report:report -B -q
echo "Done."

cd ..
echo "Copying ./nordlicht-public/* to ./sonarqube-analysis-input/"
cp -r ./nordlicht-public/* ./sonarqube-analysis-input/

# Wait 3 second to calm down everything...
echo "Waiting to make the job done..."
echo "3 sec"
sleep 1
echo "2 sec"
sleep 1
echo "1 sec"
sleep 1
echo "Done."
